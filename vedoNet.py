#!/usr/bin/env/python
# -*- coding: utf-8 -*- 


# usage: python vedoNet_sp.py [input] [output]


import sys, re, os, glob
import numpy as np
from keras.models import Sequential, model_from_json

model_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'models')
model_arch = os.path.join(model_dir, 'vedoNet.model_arch.json')
model_weights = os.path.join(model_dir, 'vedoNet.model_weights.h5')

if not os.path.exists(model_arch):
	sys.stderr.write('[FATAL] Cannot locate the model arch file. Abort.\n')
	exit(1)
if not os.path.exists(model_weights):
	sys.stderr.write('[FATAL] Cannot locate the model weights file. Abort.\n')
	exit(1)

# load model
try:
	model = model_from_json(open(model_arch, 'rb').read())
	model.load_weights(model_weights)
except:
	sys.stderr.write('[FATAL] Failure in loading model. Abort.\n')
	exit(1)

# the input data dim
N = model.layers[0].input_shape[1]

# read the input
infile, outfile = sys.argv[1:]
X = []
sampleIDs = []
for ind, line in enumerate(open(infile, 'rb')):
	cols = line.rstrip().split('\t')
	try: A = [float(a) for a in cols[1:]]
	except:
		sys.stderr.write('[FATAL] Failure in converting input data.\n')
		exit(1)
	sampleID = cols[0]
	if len(A) != N:
		print len(A)
		sys.stderr.write('[FATAL] Dimensionality failure in input data, line %i, sample: %s\n' % (ind, sampleID))
		exit(1)
	sampleIDs.append(sampleID)
	X.append(A)

X = np.array(X).astype(np.float32)
Y = model.predict_classes(X, verbose=2)
with open(outfile, 'wb') as ofh:
	for sampleID, outcome in zip(sampleIDs, Y):
		ofh.write('%s\t%i\n' % (sampleID, outcome))

