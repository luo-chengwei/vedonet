# vedoNet #

vedoNet: a neural network-based approach to determine if an IBD patient should go through vedolizamub treatment.

### Dependencies ###

* [Python 2.7](www.python.org) or above
* [Numpy 1.08](www.numpy.org) or above
* [Keras v1.1.0](http://keras.io) or above

### How to install and run? ###

No installation effort requried. Simply copy or clone the repository to wherever you want to run the package, and follow the command line:

    python vedoNet.py [input file] [output file]

You can change vedoNet.py to the other models, such as vedoNet_sp.py, vedoNet_pw.py, etc.

### Input file format guidelines ###

As for input file's format, they should follow the general format as below:

    [sampleID_1] [data vector]
	[sampleID_2] [data vector]
	......
	[sampleID_N] [data vector]
	
You can have the number of samples, N, ranging from anywhere from 1 to infinite. 

Data vector should be the float numbers delimited by tab. For instance, the vedoNet.py input should look like this:

    P1	1.3e-05	0.00014255	4.345e-05	......	4.6
    P2	1.608e-05	0.00038558	......	4.6
    ......
	Px	0.0	0.00014667	......	3.8

Where P1, P2, ..., Px, are the sample names

### Data vector column guidelines ###

To format for the right input, follow the guidelines in the './variable_def' folder, where you can find the files:

   clinical_variates.txt
   pathway_variates.txt
   ......
   vedoNet_variates.txt
  
Those files give the definition of each column in the data vector. For instance, in 'vedoNet_variates.txt' it has line:

    k__Bacteria|p__Firmicutes|c__Clostridia|o__Clostridiales|f__Lachnospiraceae|g__Roseburia|s__Roseburia_inulinivorans
    k__Bacteria|p__Actinobacteria|c__Actinobacteria|o__Bifidobacteriales|f__Bifidobacteriaceae|g__Bifidobacterium|s__Bifidobacterium_longum
    k__Bacteria|p__Firmicutes|c__Clostridia|o__Clostridiales|f__Lachnospiraceae|g__Blautia|s__Ruminococcus_gnavus
    ...
	PWY-5686: UMP biosynthesis
    PWY-6305: putrescine biosynthesis IV
    NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)
    ...
	baseline_HGB
    baseline_HCT
    baseline_PLT
    baseline_ALB
	
The species relative abundance could be quantified using [metaPhlAn2](http://huttenhower.sph.harvard.edu/metaphlan2) and the pathway relative abundance could be quantified using [HUMANn2](http://huttenhower.sph.harvard.edu/humann2).

### Run the example case ###

In the './example' folder, there is a toy example for vedoNet.py, toy.csv, which looks like this:

    P1	2.206e-05	3.67e-05	2.183e-05	2.447e-05	7.14e-06	9.33e-06	2.052e-05	3.177e-05	4.08e-05	2.018e-05	3.123e-05	2.222e-05	5.27e-06	1.445e-05	1.036e-05	3.034e-05	2.534e-05	3.469e-05	3.498e-05	3.296e-05	2.187e-05	4.379e-05	4.237e-05	0.0	1.299e-05	2.335e-05	1.777e-05	1.754e-05	4.711e-05	2.492e-05	0.0	0.0	1.598e-05	1.56e-05	0.0	0.0	0.0	0.64645	1.79833	0.0	0.50608	0.0	0.0	1.0	6.0	13.0	21.0	69.1	60.0	12.8	11.1	36.4	346.0	3.9	0.0	0.0	1.0	6.0	13.0	21.0	69.1	60.0	12.8	11.1	36.4	346.0	3.9
    P2	6.653e-05	0.00029243	6.438e-05	0.00022361	0.00017196	7.363e-05	5.977e-05	0.00011678	5.274e-05	9.324e-05	8.584e-05	2.341e-05	5.797e-05	5.481e-05	3.73e-05	0.00011519	3.877e-05	6.177e-05	0.00016759	6.712e-05	0.00019662	0.00040055	0.00029347	1.793e-05	0.0	0.00012547	3.606e-05	8.901e-05	0.00044553	3.22e-06	2.001e-05	5.392e-05	2.056e-05	0.0	5.12444	0.40576	0.0	0.33839	0.0	0.08692	0.00631	1.0	1.0	0.0	41.0	6.0	17.0	0.1	7.0	11.2	11.4	37.3	197.0	3.9	1.0	1.0	0.0	41.0	6.0	17.0	0.1	7.0	11.2	11.4	37.3	197.0	3.9
    P3	0.0	4.247e-05	3.874e-05	0.00025682	0.00017262	1.73e-05	6.01e-06	7.602e-05	7.228e-05	0.00016377	6.155e-05	2.857e-05	1.505e-05	3.216e-05	1.464e-05	0.00014663	4.3e-05	1.059e-05	0.00020019	1.347e-05	6.116e-05	0.0004637	0.00028298	7.25e-06	0.0	0.00027699	4.955e-05	0.00016168	0.00050456	0.0	2.981e-05	0.00011656	0.0	0.0	0.35235	12.52046	0.12644	0.03041	0.0	0.18863	0.0	1.0	1.0	0.0	19.0	8.0	14.0	1.0	20.0	13.2	13.1	39.4	231.0	4.42	1.0	1.0	0.0	19.0	8.0	14.0	1.0	20.0	13.2	13.1	39.4	231.0	4.42
    P4	0.0	0.00023239	3.814e-05	0.00018449	9.766e-05	9.9e-06	8.379e-05	2.466e-05	1.566e-05	0.0001076	3.891e-05	0.0	5.33e-06	0.0	0.0	8.111e-05	0.0	2.174e-05	0.00012844	2.577e-05	1.668e-05	0.00029969	0.00029721	0.00010286	4.379e-05	0.00024803	2.409e-05	6.543e-05	0.00032732	1.939e-05	7.55e-06	6.1e-05	8.024e-05	0.0	1.35984	3.68031	0.41642	0.0	0.0	0.05437	0.0454	1.0	0.0	0.0	6.0	0.6	2.0	8.0	14.8	44.5	198.0	4.6	1.0	0.0	1.0	0.0	0.0	6.0	0.6	2.0	8.0	14.8	44.5	198.0	4.6	1.0	0.0
    P5	0.0	9.888e-05	5.65e-06	0.00014443	7.24e-05	0.0	0.0	0.00014554	0.0	0.000142	5.056e-05	0.0	0.0	0.0	0.0	7.732e-05	0.0	5.42e-06	9.613e-05	6.32e-06	0.00021722	0.00039333	0.00024838	7.396e-05	0.0	9.215e-05	0.0	0.0	0.00044074	0.0	2.025e-05	0.0	2.514e-05	3.62e-05	0.0	31.05863	4.66638	0.0	0.0	0.57105	0.60665	1.0	0.0	1.0	29.0	5.0	9.0	1.7	2.0	10.7	14.6	46.3	432.0	4.7	1.0	0.0	1.0	29.0	5.0	9.0	1.7	2.0	10.7	14.6	46.3	432.0	4.7
    P6	6.177e-05	0.00027094	4.742e-05	0.0001468	5.034e-05	5.888e-05	6.977e-05	7.94e-05	5.582e-05	7.205e-05	7.874e-05	0.0	3.448e-05	2.665e-05	1.154e-05	7.367e-05	0.0	9.344e-05	9.91e-05	9.628e-05	0.00014118	0.00014205	0.00018767	5.148e-05	6.578e-05	0.00011748	5.344e-05	4.234e-05	0.0001572	5.346e-05	0.0	2.625e-05	8.975e-05	0.0	0.0	0.0	0.08607	0.0	0.0	0.60865	0.0	0.0	1.0	0.0	66.0	5.0	3.0	70.5	95.0	11.04	10.7	35.2	573.0	3.8	0.0	1.0	0.0	66.0	5.0	3.0	70.5	95.0	11.04	10.7	35.2	573.0	3.8
    P7	1.71e-06	0.00012976	7.177e-05	0.00025949	0.00018998	6.195e-05	5.011e-05	0.0001017	3.212e-05	0.00010855	3.041e-05	5.591e-05	3.973e-05	3.755e-05	1.848e-05	9.793e-05	7.214e-05	4.328e-05	0.00014485	5.033e-05	0.00014426	0.00031444	0.00025324	1.741e-05	3.917e-05	0.00021622	7.612e-05	0.00011124	0.00034072	2.529e-05	1.822e-05	0.00011993	5.402e-05	0.0	4.57512	0.93394	0.52428	0.03643	0.0	0.02109	0.89893	1.0	0.0	0.0	31.0	4.0	4.0	3.6	54.0	6.7	13.5	41.7	595.0	5.0	1.0	0.0	0.0	31.0	4.0	4.0	3.6	54.0	6.7	13.5	41.7	595.0	5.0
    P8	0.0	4.661e-05	4.304e-05	0.00022637	0.00012774	3.99e-05	0.0	7.946e-05	2.102e-05	7.294e-05	4.491e-05	3.031e-05	2.347e-05	0.0	0.0	2.174e-05	4.341e-05	2.27e-05	3.785e-05	3.048e-05	0.00011436	0.00023539	0.00028255	1.05e-06	3.202e-05	0.00019754	4.2e-05	8.524e-05	0.00025388	1.603e-05	1.35e-06	0.0001395	2.22e-05	0.0	0.9459	0.0	0.21534	0.08943	0.0	0.0	0.0	1.0	0.0	0.0	21.0	10.0	8.0	14.6	18.0	8.3	13.2	41.6	275.0	4.1	1.0	0.0	0.0	21.0	10.0	8.0	14.6	18.0	8.3	13.2	41.6	275.0	4.1
    P9	1.646e-05	0.00018277	3.724e-05	4.558e-05	8.031e-05	1.005e-05	7.414e-05	1.955e-05	9.023e-05	1.936e-05	7.355e-05	0.0	5.13e-06	6.863e-05	3.334e-05	0.00011751	0.0	9.177e-05	0.00019199	0.00010799	1.084e-05	0.00021445	0.00010969	0.00014289	4.983e-05	1.386e-05	0.00013689	7.445e-05	0.0002412	4.968e-05	0.0	9.66e-06	4.044e-05	0.0	0.0	0.27711	0.63224	0.02226	0.0	0.0	0.00103	0.0	0.0	2.0	13.0	2.0	53.0	1.7	13.0	7.29	16.2	46.3	201.0	4.2	0.0	0.0	2.0	13.0	2.0	53.0	1.7	13.0	7.29	16.2	46.3	201.0	4.2
	
To run the vedoNet predictor, in command line, run:

    python vedoNet.py example/toy.csv example/toy.output.txt
	
The toy.output.txt will have the predicted treatment outcome:

    P1	1
    P2	0
    P3	0
    P4	1
    P5	0
    P6	1
    P7	0
    P8	0
    P9	1

In which remission was labeled as 1 and non-remitter was labeled 0. Therefore, vedoNet predicts that P1, P4, P6, and P9 would enter remission.

### Citation ###
<TBD>
We are working on the manuscript now.